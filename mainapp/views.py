from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.contenttypes.models import ContentType
from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.views.generic import DetailView, View
from .models import Notebook, Smartphone, Category, LatestProducts, Customer, Cart, CartProduct
from .mixins import CategoryDetailMixin


class HomepageView(LoginRequiredMixin, View):

    @staticmethod
    def get(request, *args, **kwargs):
        customer = Customer.objects.get(user=request.user)
        cart = Cart.objects.get(owner=customer)
        categories = Category.objects.get_categories_for_left_sidebar()
        products = LatestProducts.objects.get_products_for_main_page('notebook', 'smartphone')
        context = {
            'categories': categories,
            'products': products,
            'cart': cart
        }
        return render(request, 'base.html', context)


class ProductDetailView(LoginRequiredMixin, CategoryDetailMixin, DetailView):
    context_object_name = 'product'
    template_name = 'details_products.html'
    slug_url_kwarg = 'slug'
    content_type_model_model_class = {
        'notebook': Notebook,
        'smartphone': Smartphone,
    }

    def dispatch(self, request, *args, **kwargs):
        self.model = self.content_type_model_model_class[kwargs['content_type_model']]
        self.queryset = self.model.objects.all()
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['content_type_model'] = self.model._meta.model_name
        return context


class CategoryDetailView(LoginRequiredMixin, CategoryDetailMixin, DetailView):
    model = Category
    queryset = Category.objects.all()
    context_object_name = 'category'
    template_name = 'category_detail.html'
    slug_url_kwarg = 'slug'


class AddToCartView(LoginRequiredMixin, View):

    @staticmethod
    def get(request, *args, **kwargs):
        content_type_model = kwargs.get('content_type_model')
        product_slug = kwargs.get('slug')
        customer = Customer.objects.get(user=request.user)
        cart = Cart.objects.get(owner=customer, in_order=False)
        content_type = ContentType.objects.get(model=content_type_model)
        products = content_type.model_class().objects.get(slug=product_slug)
        cart_products, created = CartProduct.objects.get_or_create(
            user=cart.owner, cart=cart, content_type=content_type, object_id=products.id
        )
        if created:
            cart.product.add(cart_products)
        return HttpResponseRedirect('/cart/')


class CartView(LoginRequiredMixin, View):

    @staticmethod
    def get(request, *args, **kwargs):
        customer = Customer.objects.get(user=request.user)
        cart = Cart.objects.get(owner=customer)
        categories = Category.objects.get_categories_for_left_sidebar()
        context = {
            'cart': cart,
            'categories': categories
        }
        return render(request, 'cart.html', context)
