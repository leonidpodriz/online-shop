from django.contrib import admin
from django.utils.safestring import mark_safe
from mainapp.models import *
from django.forms import ModelChoiceField, ModelForm
from PIL import Image


class SmartphoneAdminForm(ModelForm):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        instance = kwargs.get('instance')
        if not instance.sd:
            self.fields['sd_volume'].widget.attrs.update({
                'readonly': True, 'style': 'background: lightgray'
            })

    def clean(self):
        if not self.cleaned_data['sd']:
            self.cleaned_data['sd_volume'] = None
        return self.cleaned_data


class NotebookAdminForm(ModelForm):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['image'].help_text = mark_safe(
            '<span style="color: blue;">Загружайте изображения с минимальным разширением {}x{}</span>'.format(
                *Product.min_resolution))

    def clean_img(self):
        image = self.cleaned_data['image']
        img = Image.open(image)
        min_height, min_width = Product.min_resolution
        max_height, max_width = Product.max_resolution

        if image.size > Product.max_image_size:
            raise ValidationError('Размер изображения не должен превышать 3мб')

        if img.height < min_height or img.width < min_width:
            raise ValidationError('Загруженное изображение меньше допустимого')

        if img.height > max_height or img.width > max_width:
            raise ValidationError('Загруженное изображение больше допустимого')

        return image


class NotebookAdmin(admin.ModelAdmin):
    form = NotebookAdminForm

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == 'category':
            return ModelChoiceField(Category.objects.filter(slug='notebooks'))
        return super().formfield_for_foreignkey(db_field, request, **kwargs)


class SmartphoneAdmin(admin.ModelAdmin):
    change_form_template = 'admin.html'
    form = SmartphoneAdminForm

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == 'category':
            return ModelChoiceField(Category.objects.filter(slug='smartphones'))
        return super().formfield_for_foreignkey(db_field, request, **kwargs)


admin.site.register(Category)
admin.site.register(Notebook, NotebookAdmin)
admin.site.register(Smartphone, SmartphoneAdmin)
admin.site.register(CartProduct)
admin.site.register(Cart)
admin.site.register(Customer)
